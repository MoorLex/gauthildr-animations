import Vue from 'vue'
import Router from 'vue-router'

const _import = file => () => import(`./templates/${file}`);

Vue.use(Router);

export default new Router({
  linkActiveClass: 'active',
  hashbang: false,
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'Home',
      component: _import('Home'),
    },

    {
      path: '/waves',
      name: 'Waves',
      component: _import('Waves'),
    },
    {
      path: '/rainbow_stars',
      name: 'Rainbow_Stars',
      component: _import('Rainbow_Stars'),
    },
    {
      path: '/particles',
      name: 'Particles',
      component: _import('Particles'),
    },
    {
      path: '/maze',
      name: 'Maze',
      component: _import('Maze'),
    },
    {
      path: '/new',
      name: 'Noise',
      component: _import('Noise'),
    }
  ]
});
